package br.upis;

public interface IHorario {
	
	void setHora(byte hora);

	byte getHora();

	void setMinuto(byte minuto);

	byte getMinuto();

	void setSegundo(byte segundo);

	byte getSegundo();

	String toString();

	void incrementaSegundo();

	void incrementaMinuto();

	void incrementaHora();

	void incrementaSegundo(int n);

	boolean ehUltimoHorario();

	boolean ehPrimeiroHorario();


}
