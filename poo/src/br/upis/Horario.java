package br.upis;

public class Horario implements IHorario {

		private byte hora; 		// {0 .. 23}
		private byte minuto; 	// {0 .. 59}
		private byte segundo; 	// {0 .. 59}
		
		public Horario() {
			setHora((byte)0);
			setMinuto((byte)0);
			setSegundo((byte)0);
		}
		
		public Horario(byte hora, byte minuto, byte segundo){
			setHora(hora);
			setMinuto(minuto);
			setSegundo(segundo);
		}
		
		public Horario(int hora, int minuto, int segundo) {
			this((byte)hora, (byte)minuto, (byte)segundo);
		}

		public Horario(IHorario horario) {
			this(horario.getHora(), horario.getMinuto(), horario.getSegundo());
		}

		@Override
		public void setHora(byte hora) {
			
			if(hora >= 0 && hora <= 23) {
				this.hora = hora;
			}
		}
		
		@Override
		public byte getHora() {
			return this.hora;
		}		
	
		@Override
		public void setMinuto(byte minuto) {
			if(minuto >= 0 && minuto <= 59) {
				this.minuto = minuto;
			}
		}
		
		@Override
		public byte getMinuto() {
			return this.minuto;
		}
		
		@Override
		public void setSegundo(byte segundo) {
			if(segundo >= 0 && segundo <= 59) {
				this.segundo = segundo;
			}
		}
		
		@Override
		public byte getSegundo() {
			return this.segundo;
		}
		
		@Override
		public void incrementaSegundo() {
			
			byte s = (byte)(segundo + 1);
			
			if(s == 60) {
				segundo = 0;
				incrementaMinuto();
			}else {
				segundo = s;
			}
		}
		
		@Override
		public void incrementaMinuto() {
			byte m = (byte)(minuto + 1);
			
			if(m == 60) {
				minuto = 0;
				incrementaHora();
			}else {
				minuto = m;
			}
		}

		@Override
		public void incrementaHora() {
			byte h = (byte)(hora + 1);
			
			if(h == 24) {
				hora = 0;
			}else {
				hora = h;
			}			
		}
		
		@Override
		public void incrementaSegundo(int n) {
			
			int i = 0;
			
			while(n > 0 && i < n) {
				this.incrementaSegundo();
				i++;
			}
		}
		
		@Override
		public boolean ehUltimoHorario() {
			return hora == 23 && minuto == 59 && segundo == 59;
		}

		@Override
		public boolean ehPrimeiroHorario() {
			return hora == 0 && minuto == 0 && segundo == 0;
		}

		@Override
		public String toString() {
			return getHora() + ":" + getMinuto() + ":" + getSegundo();
		}
		
		private boolean equalsHora(Horario hr) {
			return this.getHora() == hr.getHora();
		}

		private boolean equalsSegundo(Horario hr) {
			return this.getSegundo() == hr.getSegundo();
		}
		
		private boolean equalsMinuto(Horario hr) {
			return this.getMinuto() == hr.getMinuto();
		}

		@Override
		public boolean equals(Object obj) {
			
			if(this == obj) //reflexividade
	            return true;
			
			 if(obj == null || obj.getClass()!= this.getClass()) //consistência
				 return false;
			
			 Horario hr = (Horario) obj;
			 
			 //simetria e a transitividade
			 return equalsHora(hr) && 
					equalsMinuto(hr) &&
					equalsSegundo(hr);
		}
		
		public boolean menor(Horario hr) {
			
			if(this.getHora() < hr.getHora()) {
				return true;
			}
			
			if(equalsHora(hr) && this.getMinuto() < hr.getMinuto()){
				return true;
			}
			
			if(equalsHora(hr) && equalsMinuto(hr) && this.getSegundo() < hr.getSegundo()){
				return true;
			}
			
			return false;
		}
		
		public boolean menorIgual(Horario hr) {
			if(this.getHora() <= hr.getHora()) {
				return true;
			}
			if(equalsHora(hr) && this.getMinuto() <= hr.getMinuto()) {
				return true;
			}
			if(equalsHora(hr) && equalsMinuto(hr) && this.getSegundo()<= hr.getSegundo()) {
				return true;
			}
			return false;
		}
		
		public boolean maior(Horario hr) {
			if(this.getHora() > hr.getHora()) {
				return true;
			}
			if(equalsHora(hr) && this.getMinuto() > hr.getMinuto()) {
				return true;
			}
			if(equalsHora(hr) && equalsMinuto(hr) && this.getSegundo() > hr.getSegundo()) {
				return true;
			}
			return false;
		}
		
		public boolean maiorIgual(Horario hr) {
			if(this.getHora() >= hr.getHora()) {
				return true;
			}
			if(equalsHora(hr) && this.getMinuto() >= hr.getMinuto()) {
				return true;
			}
			if(equalsHora(hr) && equalsMinuto(hr) && this.getSegundo() >= hr.getSegundo()) {
				return true;
			}
			return false;
		}
		
}
