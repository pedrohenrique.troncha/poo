package br.upis2;


public class Horario  {
	private int segundo;


public Horario() {
	setSegundo((byte)0);
}	

public Horario (byte hora, byte minuto, byte segundo) {
	setHora(hora);
	setMinuto(minuto);
	setSegundo(segundo);
}

public void setHora(byte hora) {
	if(hora >= 0 && hora <= 23) {
		this.segundo = segundo + hora*3600;
	}
}

public void setMinuto(byte minuto) {
	if(minuto >= 0 && minuto <= 59) {
		this.segundo = segundo + minuto * 60;
	}
}

public void setSegundo(int i) {
	if(i > 0 && i <= 59) {
		this.segundo = segundo + i;
	}
}

public int getHora() {
	int hora;
	hora = (segundo /3600);
	return hora;
}

public int getMinuto() {
	int minuto;
	int hora;
	hora = (segundo / 3600);
	minuto = (segundo - hora * 3600) / 60;
	return minuto;
}

public int getSegundo() {
	int seg;
	int minuto;
	int hora;
	hora = (segundo / 3600);
	minuto = (segundo - hora*3600) / 60;
	seg = (segundo - hora* 360 - minuto*60);
	return seg;
}

public String toString() {
	return getHora() + ":" + getMinuto() + ":" + getSegundo();
}	

public void incrementaSegundo() {
	if(segundo < 83400) {
		segundo = segundo +1;
	}
}

public void incrementaMinuto() {
	if(segundo/60 < 1440) {
		segundo = segundo + 60;
	}
}

public void incrementaHora() {
	if(segundo / 3600 < 24) {
		segundo = segundo + 3600;
	}
}

public boolean ehUltimoSegundo() {
	return segundo == 86399;
}

public boolean ehPrimeiroSegungo() {
	return segundo == 1;
}


}
























